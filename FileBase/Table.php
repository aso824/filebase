<?php

namespace FileBase;

use FileBase\Clause\OrderBy;
use FileBase\Exceptions\TableFileException;

class Table
{
    /**
     * Character that separates fields in data file.
     *
     * @var string
     */
    public const FIELD_SEPARATOR = '|';

    /**
     * Table name.
     *
     * @var string
     */
    protected $tableName;

    /**
     * Path to file with table.
     *
     * @var string
     */
    protected $path;

    /**
     * Handle to file.
     *
     * @var resource
     */
    protected $file;

    /**
     * Table columns.
     *
     * @var array
     */
    protected $columns = [];

    /**
     * FileTable constructor.
     *
     * @param string $tableName
     * @param string $path
     */
    public function __construct(string $tableName, string $path)
    {
        $this->tableName = $tableName;
        $this->path = $path;

        $this->loadFile();
    }

    /**
     * FileTable destructor.
     */
    public function __destruct()
    {
        if ($this->file) {
            flock($this->file, LOCK_UN);
            fclose($this->file);
        }
    }

    /**
     * Open handle to database file.
     *
     * @return void
     *
     * @throws \FileBase\Exceptions\TableFileException
     */
    protected function loadFile(): void
    {
        $this->file = @fopen($this->path, 'rb+');

        if (! $this->file) {
            throw new TableFileException(sprintf('Could not open table file [%s]', $this->path));
        }

        flock($this->file, LOCK_EX);

        $this->loadHeader();
    }

    /**
     * Load table header.
     *
     * @return void
     */
    protected function loadHeader(): void
    {
        $this->columns = $this->getRow();
    }

    /**
     * Get single row from file handle.
     *
     * @return array|null Row or null if end of file reached
     */
    protected function getRow(): ?array
    {
        $result = fgetcsv($this->file, 0, self::FIELD_SEPARATOR);

        if (false === $result) {
            return null;
        }

        return $result;
    }

    /**
     * Select rows.
     *
     * @param array $where Associative array, where key is column and value is desired value
     * @param \FileBase\Clause\OrderBy|null $orderBy
     *
     * @return array
     */
    public function select(array $where = [], OrderBy $orderBy = null): array
    {
        $result = [];

        while ($row = $this->getRow()) {
            $row = array_combine($this->columns, $row);

            if (! empty($where) && ! $this->applyWheres($row, $where)) {
                continue;
            }

            $result[] = $row;
        }

        if ($orderBy) {
            uasort($result, function (array $a, array $b) use ($orderBy): int {
                if ($orderBy->getDirection() === OrderBy::ORDER_ASCENDING) {
                    return $a[$orderBy->getColumn()] <=> $b[$orderBy->getColumn()];
                }

                return $b[$orderBy->getColumn()] <=> $a[$orderBy->getColumn()];
            });
        }

        $this->resetFilePointer();

        return $result;
    }

    /**
     * Apply WHERE clauses to row.
     *
     * @param array $row
     * @param array $where
     *
     * @return bool
     */
    protected function applyWheres(array $row, array $where): bool
    {
        foreach ($row as $key => $value) {
            // Use non-strict comparision because there is no types in database and all values is read as string
            if (isset($where[$key]) && $value != $where[$key]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Seek pointer in file to second line (omit header).
     *
     * @return void
     */
    protected function resetFilePointer(): void
    {
        rewind($this->file);
        $this->skipLines(1);
    }

    /**
     * Seek pointer in file, ignoring given count of lines.
     *
     * @param int $lineCount
     *
     * @return void
     */
    protected function skipLines(int $lineCount): void
    {
        while ($lineCount--) {
            // If getRow() return null, we want to break loop (not need to loop anymore - EOF reached)
            if (! $this->getRow()) {
                break;
            }
        }
    }

    /**
     * Seek file to the end.
     *
     * @return void
     */
    protected function seekEnd(): void
    {
        // @TODO: implement method
    }

    /**
     * Insert row.
     *
     * @param array $data Associative array, where key is column name, and value is desired value.
     *
     * @return void
     */
    public function insert(array $data): void
    {
        // New rows will be appended at the end
        $this->seekEnd();

        // @TODO: implement method
    }

    /**
     * Update values in table.
     *
     * @param array $data
     * @param array $where
     *
     * @return int Count of modified rows
     */
    public function update(array $data, array $where = []): int
    {
        // @TODO: implement method
    }

    /**
     * Delete values from table.
     *
     * @param array $where
     *
     * @return int Count of deleted rows
     */
    public function delete(array $where): int
    {
        // @TODO: implement method
    }
}
