<?php

namespace FileBase\Clause;

class OrderBy
{
    /**
     * Parameter for sorting ascending.
     *
     * @var string
     */
    public const ORDER_ASCENDING = 'asc';

    /**
     * Parameter for sorting descending.
     *
     * @var string
     */
    public const ORDER_DESCENDING = 'desc';

    /**
     * Available sort directions.
     *
     * @var array
     */
    public const ORDER_DIRECTIONS = [
        self::ORDER_ASCENDING,
        self::ORDER_DESCENDING,
    ];

    /**
     * Column name to sort.
     *
     * @var string
     */
    protected $column;

    /**
     * Direction of sorting.
     *
     * @var string
     */
    protected $direction;

    /**
     * OrderBy constructor.
     *
     * @param string $column
     * @param string $direction
     */
    public function __construct(string $column, string $direction = self::ORDER_ASCENDING)
    {
        $this->throwIfInvalidDirection($direction);

        $this->column = $column;
        $this->direction = $direction;
    }

    /**
     * Check if given order direction is valid - if not, throw an exception.
     *
     * @param string $direction
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    protected function throwIfInvalidDirection(string $direction): void
    {
        if (! in_array($direction, self::ORDER_DIRECTIONS, true)) {
            throw new \InvalidArgumentException('Invalid order direction.');
        }
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }

    /**
     * @param string $column
     *
     * @return void
     */
    public function setColumn(string $column): void
    {
        $this->column = $column;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     *
     * @return void
     */
    public function setDirection(string $direction): void
    {
        $this->direction = $direction;
    }
}
