<?php

namespace FileBase;

use FileBase\Exceptions\TableNotFoundException;

class Database
{
    /**
     * Extension used for table files.
     *
     * @var string
     */
    public const TABLE_FILE_EXTENSION = 'txt';

    /**
     * Directory where data is stored.
     *
     * @var string
     */
    protected $dataDirectory;

    /**
     * Database constructor.
     *
     * @param string $dataDirectory
     */
    public function __construct(string $dataDirectory)
    {
        $this->dataDirectory = $dataDirectory;
    }

    /**
     * Get table.
     *
     * @param string $tableName
     *
     * @return \FileBase\Table
     *
     * @throws \FileBase\Exceptions\TableNotFoundException
     */
    public function getTable(string $tableName): Table
    {
        $path = $this->getPathToTable($tableName);

        if (! is_file($path) || ! is_readable($path)) {
            throw new TableNotFoundException(sprintf('Table [%s] was not found.', $tableName));
        }

        return new Table($tableName, $path);
    }

    /**
     * Get path to table file.
     *
     * @param string $tableName
     *
     * @return string
     */
    protected function getPathToTable(string $tableName): string
    {
        return $this->dataDirectory . DIRECTORY_SEPARATOR . $tableName . '.' . self::TABLE_FILE_EXTENSION;
    }

    /**
     * Create new table.
     *
     * @param string $tableName
     *
     * @return \FileBase\Table
     */
    public function createTable(string $tableName): Table
    {
        // @TODO: implement method
    }

    /**
     * Drop given table.
     *
     * @param string $tableName
     *
     * @return void
     *
     * @throws \FileBase\Exceptions\TableNotFoundException
     */
    public function dropTable(string $tableName): void
    {
        // @TODO: implement method
    }

    /**
     * @return string
     */
    public function getDataDirectory(): string
    {
        return $this->dataDirectory;
    }
}
