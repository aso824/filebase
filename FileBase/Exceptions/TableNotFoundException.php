<?php

namespace FileBase\Exceptions;

class TableNotFoundException extends \InvalidArgumentException
{
}
