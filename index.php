<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'FileBase/Exceptions/InvalidDataException.php';
require_once 'FileBase/Exceptions/TableFileException.php';
require_once 'FileBase/Exceptions/TableNotFoundException.php';
require_once 'FileBase/Clause/OrderBy.php';
require_once 'FileBase/Table.php';
require_once 'FileBase/Database.php';

$database = new \FileBase\Database('data');
$table = $database->getTable('testowa');

echo 'Wszystkie wiersze: ';
var_dump(
    $table->select()
);

echo 'Tylko 23 latkowie: ';
var_dump(
    $table->select([
        'wiek' => '23',
    ])
);

echo 'Według wieku: ';
var_dump(
    $table->select(
        [],
        new \FileBase\Clause\OrderBy('wiek', \FileBase\Clause\OrderBy::ORDER_ASCENDING)
    )
);
